<?php

/**
 * Plugin Name: wpsync-webspark
 * Description: wpsync-webspark
 * Version: 1
 * Author:      Poroskun Denys
 * Text Domain: wpsync-webspark
 * Domain Path: /languages/
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Webspark {

    private static $instance = null;

    public static function instance() {
        if ( null == self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function __construct() {

        $this->init();
    }

    public function init() {
        if ( ! wp_next_scheduled( 'hourly_update' ) ) {
            wp_schedule_event( time(), 'hourly', 'hourly_update' );
        }
        add_action( 'hourly_update', array( $this, 'load_file' ) );
        add_action( 'update_product_event', array( $this, 'update_product' ), 10, 3 );
        add_action( 'woocommerce_checkout_create_order', array( $this, 'create_order' ), 10, 2 );
        //add_action( 'init', array( $this, 'load_file' ) );
    }

    public function load_file() {

        $api  = json_decode( file_get_contents( "https://my.api.mockaroo.com/products.json?key=89b23a40" ) );
        $date = current_time( 'timestamp' );
        wp_schedule_single_event( time(), 'update_product_event', array( $api, 0, $date ) );
    }

    public function update_product( $api, $index, $date ) {
        for ( $i = $index; $i < $index + 10; $i ++ ) {
            $value = $api[ $i ];
            if ( empty( $value ) ) {
                $posts = get_posts( array(
                    'post_type'  => 'product',
                    'meta_query' => array(
                        array(
                            'key'     => 'curent_date',
                            'value'   => $date,
                            'compare' => '!='
                        )
                    )
                ) );
                foreach ( $posts as $post ) {
                    wp_delete_post( $post->ID );
                }
                exit;
            }
            $product_id = wc_get_product_id_by_sku( $value->sku );

            if ( empty( $product_id ) ) {
                $objProduct = new WC_Product();

                $objProduct->set_name( $value->name );
                $objProduct->set_status( "publish" );
                $objProduct->set_catalog_visibility( 'visible' );
                $objProduct->set_description( $value->description );
                $objProduct->set_sku( $value->sku );
                $objProduct->set_price( $value->price );
                $objProduct->set_regular_price( $value->price );
                $objProduct->set_manage_stock( true );
                $objProduct->set_stock_quantity( $value->in_stock );
                $objProduct->set_stock_status( 'instock' );

                $productImagesIDs = array();

                $mediaID            = $this->uploadMedia( $value->picture );
                if ( $mediaID )
                    $productImagesIDs[] = $mediaID;

                if ( $productImagesIDs ) {
                    $objProduct->set_image_id( $productImagesIDs[ 0 ] );
                }
                $id = $objProduct->save();

                update_post_meta( $id, 'curent_date', $date );
            } else {
                $my_post[ 'ID' ]           = $product_id;
                $my_post[ 'post_title' ]   = $value->name;
                $my_post[ 'post_content' ] = $value->description;
                wp_update_post( wp_slash( $my_post ) );
                update_post_meta( $product_id, '_price', $value->price );
                update_post_meta( $product_id, '_regular_price', $value->price );
                update_post_meta( $product_id, '_stock', $value->in_stock );
                update_post_meta( $product_id, 'curent_date', $date );
            }
        }
        if ( $i >= 10000 ) {
            exit;
        }

        wp_schedule_single_event( time(), 'update_product_event', array( $api, $i, $date ) );
    }

    public function uploadMedia( $image_url ) {
        require_once ABSPATH . 'wp-admin/includes/media.php';
        require_once ABSPATH . 'wp-admin/includes/file.php';
        require_once ABSPATH . 'wp-admin/includes/image.php';
        $media       = media_sideload_image( $image_url, 0 );
        $attachments = get_posts( array(
            'post_type'   => 'attachment',
            'post_status' => null,
            'post_parent' => 0,
            'orderby'     => 'post_date',
            'order'       => 'DESC'
        ) );
        return $attachments[ 0 ]->ID;
    }

    public function create_order( $order, $data ) {
        foreach ( $order->get_items() as $item ) {
            $product  = $item->get_product();
            $quantity = $item->get_quantity();
            $sku      = $product->get_sku();
            $array[]  = array(
                'sku'   => $sku,
                'items' => $quantity
            );
        }

        $data_string = json_encode( $array, JSON_UNESCAPED_UNICODE );
        $curl        = curl_init( 'https://my.api.mockaroo.com/order.json?key=89b23a40&__method=POST' );
        curl_setopt( $curl, CURLOPT_POST, 1 );
        curl_setopt( $curl, CURLOPT_POSTFIELDS, $data_string );

        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen( $data_string ) )
        );

        $result = curl_exec( $curl );

        $status = json_decode( $result );
        if ( $status->status != 'accepted' ) {
            $cart                  = new WC_Cart();
            $cart->empty_cart( $clear_persistent_cart = true );
            exit;
        }
        curl_close( $curl );
    }

}

function Webspark() {
    return Webspark::instance();
}

Webspark();
